import React from 'react';
import "./MainPanel.css";
import students from "../../classroom_data.json"

export default class MainPanel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      students: students,
      class: []
    }
  }


  componentDidMount() {
    let a = []
    students.map(element => {
      if(element.classname == this.props.match.params.name){
        a = [ ...element.students]
      }
      
    });
    this.setState({
      class: a
    })
  }


  render() {
    // {console.log("req", this.props.match.params.name)}
    return (
      // console.log("now", this.state.class)
      <div>
        <h1> {this.state.students.classname} </h1>
        {this.state.class.map(student => {
          return (
            <div className="card text-white bg-dark border-success mb-2 offset-4">
                <p><b>{student.name}</b><span> Average: {parseInt((student.marks.English + student.marks.Maths + student.marks.Science) / 3 )}%</span></p>
                <p>English: {student.marks.English}</p>
                <p>Maths: {student.marks.Maths}</p>
                <p>Science: {student.marks.Science}</p>
            </div>
          )
        })}

          
      </div>
    );
  }
}
