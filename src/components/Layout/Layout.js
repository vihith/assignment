import React from 'react';

import MainPanel from '../MainPanel/MainPanel';
import SidePanel from '../SidePanel/SidePanel';

export default class Layout extends React.Component {
  constructor(props) {
    super(props)
    this.state= {
      isClicked: false,
      cls: ''
    }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick = (e)=>{
    this.setState({
      isClicked: true,
      cls: e.target.name
    })
  }

  render() {
    console.log(this.state)
    return (
      <div className="layout">
        <div className="row">
          <div className="col-md-3">
          <SidePanel  handleClick={this.handleClick}/>
          </div>
            {this.state.isClicked &&<div className="col-md-4"> <MainPanel/> </div>}
          </div>
            

       
      </div>
    );
  }
}
