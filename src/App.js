import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './App.css';
import Layout from './components/Layout/Layout';
import data from './classroom_data.json';

import SidePanel from './components/SidePanel/SidePanel'
import MainPanel from './components/MainPanel/MainPanel';

function App(props) {
    console.log({data}); // Use this data to render this page
    return (
      <BrowserRouter>
      <div className="container">
        <Layout />
      </div>
      <Switch>
        <Route path='/class' component={SidePanel} exact={true} />
        <Route path={`/:name`} component={MainPanel} exact={true}/>

      </Switch>

      </BrowserRouter>
    );
}

export default App