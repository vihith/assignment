import React from 'react';
import "./SidePanel.css"
import { Link, BrowserRouter } from 'react-router-dom'
import schools from "../../classroom_data.json"

class SidePanel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      schools : schools
    }
  }


  render() {
    return (
      <BrowserRouter>
      <div className="sidepanel row">
      <div className="sidepanel col col-md-11">
        <h1> School </h1>
        {this.state.schools.length && this.state.schools.map(school => {
          return <Link className="sidepanel text-dark col-md-4" name={school.classname} to={`/${school.classname}`} onClick={this.props.handleClick}><li><b>{school.classname}</b></li></Link>
        })}
      </div>
      </div>
      </BrowserRouter>
    );
  }
}


export default SidePanel 